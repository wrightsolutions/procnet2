#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2012-2018 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause

from os import getenv as osgetenv
from sys import exit,argv,stdin
import re
import shlex
from subprocess import Popen,PIPE
#import subprocess


# python ./ncon_from_conf_warnonmax1.py /root/.pand/pand.conf
# egrep 'maxconnections\s*=' /root/.pand/pand.conf | ./ncon_from_conf_warnonmax1.py 'stdin'

# chr(58) is colon (:)	; chr(61) is equals (=)	; chr(32) is space
re_keyval_equals_wordy = re.compile("[a-zA-Z0-9_]+\s*{0}\s*\w+".format(chr(61)))
#re_keyval_colon_wordy = re.compile("^[a-zA-Z0-9_]+\s*{0}\s*\w+".format(chr(58)))
DOTTY=chr(46)
#DOUBLE_DOT=chr(46)*2
# chr(46) is dot (.)	; chr(47) is forward slash (/)
re_dot_or_slash = re.compile("{0}|{1}".format(DOTTY,chr(47)))


keyval_list = []


def process_from_list(lines):
	""" Created with test harnesses in mind ... just give it a list of lines
	"""
	global keyval_list
	line_count = 0
	for line in lines:
		if len(line.strip()) < 3:
			# blank lines do not increment the count
			continue
		# Something worth processing. We rstrip().
		line_s = line.rstrip()
		if re_keyval_equals_wordy.match(line_s):
			keyval_list.append(line_s)
		line_count += 1
	return line_count


def process_from_conf(cfile):
	""" Reads a conf file which is expected to contain some key val entries
	Call process_from_list() to establish nonblanks and regex test the lines
	Return code of < 0 indicates a problem opening the conf file for reading
	"""
	global keyval_list
	try:
		with open(cfile, 'r') as f:
			lines = f.read().splitlines()
			nonblanks_count = process_from_list(lines)
	except IOError:
		nonblanks_count = -1
	return nonblanks_count



if __name__ == '__main__':

	program_binary = argv[0].strip()
	from os import path
	program_dirname = path.dirname(program_binary)

	conf_file_str = argv[1]
	conf_file = None
	if 'stdin' == conf_file_str.strip():
		conf_file = 'stdin'
	elif re_dot_or_slash.search(conf_file_str):
		conf_file = conf_file_str.strip()
	else:
		pass

	PYVERBOSITY_STRING = osgetenv('PYVERBOSITY')
	PYVERBOSITY = 1
	if PYVERBOSITY_STRING is None:
		pass
	else:
		try:
			PYVERBOSITY = int(PYVERBOSITY_STRING)
		except:
			pass
	#print(PYVERBOSITY)

	""" Next verbosity_global = 1 is the most likely outcome
	unless the user specifically overrides it by setting
	the environment variable PYVERBOSITY
	"""
	if PYVERBOSITY is None:
		verbosity_global = 1
	elif 0 == PYVERBOSITY:
		# 0 from env means 0 at global level
		verbosity_global = 0
	elif PYVERBOSITY > 1:
		# >1 from env means same at global level
		verbosity_global = PYVERBOSITY
	else:
		verbosity_global = 1
    
	if conf_file is None:
		if verbosity_global > 1:
			print("Conf file {0} rejected!".format(conf_file_str))
		exit(101)

	if 'stdin' == conf_file:
		# Where you may want sudo or not wanting monitoring reading conf
		nonblanks_count = process_from_list(stdin.readlines())
	else:
		# Typical case when run manually and giving conf as arg
		nonblanks_count = process_from_conf(conf_file)

	if nonblanks_count < 0:
		if verbosity_global > 1:
			print("Conf file {0} permission or read error".format(conf_file))
		exit(102)
		
	KEYVAL_NOTFOUND_STR='did not appear to contain valid key val pairs'
	if nonblanks_count < 1:
		if verbosity_global > 1:
			print("Conf file {0} {1}".format(conf_file,KEYVAL_NOTFOUND_STR))
		exit(105)

	keyval_dict = {}
	for line in keyval_list:
		array_equals = line.strip().split(chr(61))
		k = array_equals[0].strip()
		val = array_equals[1].strip()
		if len(k) < 2:
			continue
		if len(val) < 1:
			continue
		keyval_dict[k] = val

	if 'maxconnections' not in keyval_dict:
		KEYVAL_NOTFOUNDMAX = 'did not contain maxconnections key'
		if verbosity_global > 1:
			print("Conf file {0} {1}.".format(conf_file,KEYVAL_NOFOUNDMAX))
		exit(110)

	# From here we know that maxconnections is defined in the conf file (and dict)
	maxcon = None
	maxcon_str = keyval_dict['maxconnections']
	try:
		maxcon = int(maxcon_str.strip())
	except:
		pass

	if maxcon is None:
		if verbosity_global > 1:
			print("Conf file {0} {1}.".format(conf_file,KEYVAL_NOFOUNDMAX))
		exit(120)
		
	if verbosity_global > 1:
		CONSIDERED_STR='will be considered when defining our callout'
		print("maxconnections={0} {1} to .py".format(maxcon,CONSIDERED_STR))

	maxcon1 = maxcon-1
	cmd_ncon = "{0}/ncon_filter2.py --warn {1} --crit {2}".format(program_dirname,maxcon1,maxcon)
	subncon = Popen(shlex.split(cmd_ncon),bufsize=1024,stdout=PIPE)
	subout, suberr = subncon.communicate()
	# .returncode is only populated after a communicate() or poll/wait
	ncon_rc = subncon.returncode
	if verbosity_global > 1 and ncon_rc > 2:
		print("Error {0} during {1}".format(ncon_rc,cmd_ncon))
	elif verbosity_global > 2:
		if ncon_rc > 0:
			print("ncon_rc={0} from .returncode={1}".format(ncon_rc,subncon.returncode))
	else:
		pass

	for nline in subout.splitlines():
		print(nline.rstrip())

	exit(ncon_rc)

