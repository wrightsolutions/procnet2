#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2018 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause

import re
from sys import argv,exit,stdin
from os import getenv as osgetenv
from os import path,stat
from collections import defaultdict,namedtuple
#from time import gmtime
from datetime import datetime as dt

""" From output of a /sbin/route command supplied on stdin extract pattern
matches and optionally group by
Example1: route -n | python2 ./routeby.py
# Next example asks for entries to be grouped [and sorted] by 'gateway'
Example1: route -n | python2 ./routeby.py 'gateway'
# Next example asks for entries to be grouped [and sorted] by 'interface'
Example1: route -n | python2 ./routeby.py 'interface'
"""

PYVERBOSITY_STRING = osgetenv('PYVERBOSITY')
PYVERBOSITY = 1
if PYVERBOSITY_STRING is None:
	pass
else:
	try:
		PYVERBOSITY = int(PYVERBOSITY_STRING)
	except:
		pass
#print(PYVERBOSITY)

""" Next verbosity_global = 1 is the most likely outcome
unless the user specifically overrides it by setting
the environment variable PYVERBOSITY
"""
if PYVERBOSITY is None:
	verbosity_global = 1
elif 0 == PYVERBOSITY:
	# 0 from env means 0 at global level
	verbosity_global = 0
elif PYVERBOSITY > 1:
	# >1 from env means same at global level
	verbosity_global = PYVERBOSITY
else:
	verbosity_global = 1


#from string import ascii_letters,punctuation,printable
from string import printable
SET_PRINTABLE = set(printable)

progfull = argv[0].strip()
from os import path
progbase = path.basename(progfull)
progname = progbase.split('.')[0]
progdir = path.dirname(progbase)

ISOCOMPACT_STRFTIME='%Y%m%dT%H%M%S'

COLON=chr(58)
SPACER=chr(32)
TABBY=chr(9)
PARENTH_OPEN = chr(40)
PARENTH_CLOSE = chr(41)
# Above are () and next we define {}
BRACES_OPEN = chr(123)
BRACES_CLOSE = chr(125)
# Now for type that appear mostly in system files []
BRACK_OPEN = chr(91)
BRACK_CLOSE = chr(93)

"""
A	installed by addrconf
C	cache entry
D	dynamically installed by daemon or redirect
G	use gateway
H	target is a host
M	modified from routing daemon or redirect
R	reinstate route for dynamic routing
U	UP - The route is up
!	reject route
These for the basis for route flag (RFLAG) defined next
"""

PATTERN_DEFAULT="{0}\d.{1}".format(PARENTH_OPEN,PARENTH_CLOSE)
RE_PATTERN_DEFAULT = re.compile(r"{0}{1}3,3{2}\d".format(PATTERN_DEFAULT,BRACES_OPEN,BRACES_CLOSE))
RFLAG = '(A|C|D|G|H|M|R|U|!)'
RE_FLAGS = re.compile("{0}+".format(RFLAG))
RE_ZERO4 = re.compile('0.0.0.0')
RE_MASKA = re.compile('255.0.0.0')
RE_MASKB = re.compile('255.255.0.0')
RE_MASKC = re.compile('255.255.255.0')
RE_MASKD = re.compile('255.255.255.255')
RE_BRACKETED_NUM = re.compile(r"{0}\d*{1}".format(BRACK_OPEN,BRACK_CLOSE))
RE_BRACKETED_TIME_OR_NUM = re.compile(r'\[[\-T0-9.:]*\]')

RouteLine = namedtuple('RouteLine', ['dest','gw','mask','flags','metric',
				'ref','use','interface'])


def tuple_from_line(line):
	tup = None
	ln_array = line.strip().split()
	try:
		tup = RouteLine(ln_array[0],ln_array[1],ln_array[2],ln_array[3],
				ln_array[4],ln_array[5],ln_array[6],ln_array[7])
	except:
		print("Error forming tuple from line={0}".format(line))
		raise
	return tup


def ip4valid(ipv4string):
	""" Returns stripped ip4 if a valid IP version 4 address
	otherwise returns None
	"""	
	addr_return = None
	addr_stripped = ipv4string.strip()
	try:
		socket.inet_aton(addr_stripped)
		addr_return = addr_stripped
	except:
		pass
	return addr_return


def validport(port_given,port_minimum=1024):
	port_int = None
	try:
		port_int = int(port_given)
	except:
		pass
	if port_int >= TWO_TO_SIXTEEN:
		# Above maximum allowed value for port
		return None
	elif port_int < 1:
		# Too small
		return None
	elif port_int < port_minimum:
		# Below limit based on supplied arg/defaul
		return None
	else:
		# Valid port passing all limit tests
		pass

	return port_int


def lines_iter_counter(lines_iter,pattern=PATTERN_DEFAULT,verbosity=0):
	if pattern is None or len(pattern.strip()) < 2:
		pattern = PATTERN_DEFAULT

	if pattern == PATTERN_DEFAULT:
		re_pattern = RE_PATTERN_DEFAULT
	elif set(pattern).issubset(SET_PRINTABLE):
		try:
			re_pattern = re.compile(pattern)
		except:
			re_pattern = None
	else:
		re_pattern = None

	match_lines = []

	while re_pattern is not None:
		try:
			line = next(lines_iter)
		except StopIteration:
			break

		sline = line.rstrip()
		if re_pattern.search(line):
			match_lines.append(sline)

	return match_lines


def output_from_list(list_of_tuples,groupby=None,printer=False):
	"""
	RouteLine = namedtuple('RouteLine', ['dest','gw','mask','flags','metric',
				'ref','use','interface'])
	"""
	lines = []
	if groupby is None:
		for tup in list_of_tuples:
			line4 = "{1}{0}{2}{0}{3}{0}{4}".format(TABBY,tup.dest,tup.gw,
							tup.mask,tup.flags)
			line = "{5}{1}{0}{2}{0}{3}{0}{4}".format(TABBY,tup.metric,tup.ref,
							tup.use,tup.interface,line4)
			lines.append(line)
	else:
		dict_of_indexes = defaultdict(list)
		for idx,tup in enumerate(list_of_tuples):
			if 'gw' == groupby:
				dict_of_indexes[tup.gw].append(idx) 
			elif 'interface' == groupby:
				dict_of_indexes[tup.interface].append(idx)
			else:
				output_prefix = 'output could not be formed as' 
				raise Exception("{0} groupby={1} no good!".format(output_prefix,
									groupby))
		for gkey in sorted(dict_of_indexes):
			for idx in dict_of_indexes[gkey]:
				tup = list_of_tuples[idx]
				line4 = "{1}{0}{2}{0}{3}{0}{4} ".format(TABBY,tup.dest,tup.gw,
								tup.mask,tup.flags)
				line = "{5}{1}{0}{2}{0}{3}{0}{4}".format(TABBY,tup.metric,tup.ref,
								tup.use,tup.interface,line4)
				lines.append(line)

	if printer is True:
		for line in lines:
			print(line)

	return lines


if __name__ == '__main__':


	groupby = None
	if len(argv) > 1:
		groupby = argv[1]
		#if len(argv) > 2:

	# Prefer iter(stdin.readline, '') instead of iter(stdin.readlines())
	stdin_iter = iter(stdin.readline, '')
	matching_lines = lines_iter_counter(stdin_iter,'.',verbosity_global)
	#matching_lines = lines_iter_counter(stdin_iter,pattern=PATTERN_DEFAULT)
	mcount = len(matching_lines)

	if verbosity_global > 1:
		print("{0} matching lines returned from lines_iter work".format(mcount))

	nt_list = []
	for line in matching_lines:
		tup = tuple_from_line(line)
		if tup is not None:
			nt_list.append(tup)

	nt_list2 = []
	if 'interface' == groupby:
		nt_list2 = sorted(nt_list, key=lambda t: t.interface)
	elif groupby in ['gateway','gw']:
		groupby = 'gw'
		nt_list2 = sorted(nt_list, key=lambda t: t.gw)
	else:
		pass

	""" If we are groupby'ing then we will have sorted by same
	field already and results in nt_list2 for convenience
	Next is to actually DO the groupby (if applicable)
	"""

	if len(nt_list2) < 2:
		# Final arg True requests print() of output
		output_from_list(nt_list,groupby,True)
	else:
		# Output will be subject to a 'groupby'
		output_from_list(nt_list2,groupby,True)

	"""
	ip1 = ip4valid(argv[2])
	if ip1 is None:
		exit(151)
	port1 = validport(argv[3])
	if port1 is None:
		exit(152)
	"""

