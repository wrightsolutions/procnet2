# Example of input text from input/ and commands with supporting notes

Most of the commands shown here will use the argument --stdin  
as we are supplying input rather than relying on netstat call


# input/desktop9dbus.txt

```
head -n 5 input/desktop9dbus.txt | ./ncon_filter2.py --stdin
```

Above just gives you a sample of the top 5 lines from the .txt input file

