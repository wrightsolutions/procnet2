#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2012-2018 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause

import argparse
from os import getenv as osgetenv
from sys import exit,argv,stdin
from subprocess import Popen,PIPE
import shlex
import re

""" Parse typical output from programs that process /proc/net (netstat)
and attach internal labels to the processed lines.

Gives indexed output (ignoring padding)

Run with elevated privileges (root) _or_ use 2>&1 so typical
netstat warning becomes part of input to script.

Suggest running first time with PYVERBOSITY=3 as follows
export PYVERBOSITY=3

...so that you are informed of any parse issues in detail

Example usage (stdin mode):
netstat -anp -4 -6 2>&1 | python ./ncon_filter2.py --stdin

Above example includes 2>&1 so netstat warnings are part of input to script

First line of output would typically be:
2 tcp        0      0 0.0.0.0:??
...index above is 2 because of header lines. If first index if 4 you have 
...a suppressed warning in the mix also

Example that is similar to giving -4 and -6 to netstat (not interested in nix sockets)
python ./ncon_filter2.py --tcpudp

Example that is suitable for hook into monitoring system:
python ./ncon_filter2.py --warn 5 --crit 10
"""

netcmd = 'netstat -anp'
#print shlex.split(findcmd)

verbosity_global = 1
# Above is set by default to 1. Also see __name__ == '__main__' section later.

STATE_OK=0
STATE_WARNING=1
STATE_CRITICAL=2
STATE_UNKNOWN=3
STATE_DEPENDENT=4
STATE_UNKNOWN_MESSAGE='STATE UNKNOWN'
DESCRIPTION='Check if /proc/net/ entries for tcp and udp above given thresholds'

UNLABELLED_STRING='UNLABELLED'
# Above can be changed if getting clashes regularly

# chr(58) is colon (:)

active_space = re.compile('Active\s')
re_proto_re = re.compile("Proto Re")
""" Above designed to catch 'Proto Recv-Q Send-Q Local Address           Foreign Address'
or 'Proto RefCnt Flags       Type       State         I-Node'
"""
""" The next regular expression is not something I would expect to encounter,
but serves to illustrate how you would go about testing for error output from
the actual rsync command. """
re_not_all = re.compile('\(Not all processes could be identified')
re_not_all_root_required = re.compile('you would have to be root to see it all')
re_tcp6_numeric = re.compile('tcp6*\s+[0-9]+\s')
re_udp6_numeric = re.compile('udp6*\s+[0-9]+\s')
re_tcp_colons = re.compile("tcp*\s.*{0}.*{0}".format(chr(58)))
re_udp_colons = re.compile("udp*\s.*{0}.*{0}".format(chr(58)))
re_unix_numeric = re.compile('unix\s+[0-9]+\s')
re_tmp_dbus = re.compile('\s@/tmp/dbus')
re_at_dbus = re.compile('\s@/dbus')
re_var_run_dbus = re.compile('\s/var/run/dbus/')
re_dbus_hyphen = re.compile('[0-9]/dbus-')
re_dbus_run_user = re.compile('dbus.*\s+/run/user/[0-9]+/bus$')
re_run_user_bus = re.compile('\s+/run/user/[0-9]+/bus$')
re_x_related_x11 = re.compile('\s@/tmp/.X11')
re_x_related_ice = re.compile('\s@/tmp/.ICE')
re_connected_hyphen_end = re.compile('\sCONNECTED\s+[0-9]+\s+-$')
re_dgram_hyphen_end = re.compile('\sDGRAM\s+[0-9]+\s+-$')
re_connected_numeric_master = re.compile('\sCONNECTED\s+[0-9]+\s+[0-9]+/master')
re_listening_numeric_master = re.compile('\sLISTENING\s+[0-9]+\s+[0-9]+/master')
re_connected_rpc_dot = re.compile('\sCONNECTED\s+[0-9]+\s+[0-9]+/rpc\.')
re_dgram_rpc_dot = re.compile('\sDGRAM\s+[0-9]+\s+[0-9]+/rpc\.')
re_rpc_dot_alpha = re.compile('\s+[0-9]+/rpc\.[a-z]*$')
re_rpcbind = re.compile('\s+[0-9]+/rpcbind$')
re_exim = re.compile('\s+[0-9]+/exim[0-9]+$')
re_listen_exim = re.compile('LISTEN\s+[0-9]+/exim[0-9]+$')
re_listen_master = re.compile('LISTEN\s+[0-9]+/master$')
re_listen_sshd = re.compile('LISTEN\s+[0-9]+/sshd$')
re_any127 = re.compile('[0-9]+\s+127.0.0.1:[0-9]+.*[A-Z]+')
re_foreign127 = re.compile(':[0-9]+\s+127.0.0.1:[0-9]+.*[A-Z]+')

dbus_count = 0
dns_count = 0
nfs_count = 0
nonidentified_count = 0
padding_count = 0
postfix_count = 0
ssh_count = 0
warning_count = 0
warning_indexes = []
""" Warning count and warning_indexes are all about detecting
warning text/output from the original stdin or netstat command.
The two variables above are not related to the one line reporting
mode of operation and the warn_thresh_int fed from argument
"""

xrelated_count = 0
process_count = 0

processed_list = []
""" The global noutput_labelled will be used by function
noutput_label() to give char strings such as 'nfs'
based on the contents of each output line
"""
noutput_labelled = []


def process_from_command():
    global processed_list
    pout = (Popen(shlex.split(netcmd),bufsize=4096,stdout=PIPE)).stdout
    #processed_list = (Popen(netcmd.split(' '),bufsize=4096,stdout=PIPE)).stdout
    line_count = 0
    for line in pout:
        processed_list.append(line.strip())
        line_count += 1
    return line_count


def process_from_list(lines):
    """ Created with test harnesses in mind ... just give it a list of lines
    """
    global processed_list
    line_count = 0
    for line in lines:
        processed_list.append(line.strip())
        line_count += 1
    return line_count


def process_from_stdin():
    global processed_list
    line_count = 0
    for line in stdin.readlines():
        processed_list.append(line.strip())
        line_count += 1
    return line_count


def noutput_label(verbosity=0):
    global dbus_count, dns_count, nfs_count, nonidentified_count
    global padding_count, postfix_count, ssh_count, xrelated_count
    global warning_count, warning_indexes

    global noutput_labelled
    array_index = -1
    noutput_label_rc = 0
    for l in processed_list:
        array_index += 1
        line = l.strip()
        noutput_labelled.append([line,array_index])
        if re_not_all.match(line):
            if verbosity > 2:
                print("First line of typical warning about non-root was found!!")
            padding_count += 1
            warning_count += 1
            warning_indexes.append(array_index)
            noutput_labelled[array_index].append('warning')
            noutput_label_rc = 1
            continue
        elif re_not_all_root_required.search(line):
            if verbosity > 2:
                print("second line of typical warning about non-root was found!!")
            padding_count += 1
            warning_count += 1
            warning_indexes.append(array_index)
            noutput_labelled[array_index].append('warning')
            noutput_label_rc = 1
            continue
        elif active_space.match(line):
            padding_count += 1
            noutput_labelled[array_index].append('header')
            continue
        elif re_proto_re.match(line):
            padding_count += 1
            noutput_labelled[array_index].append('header')
            continue
        elif re_unix_numeric.match(line):
            if re_tmp_dbus.search(line):
                noutput_labelled[array_index].append('dbus')
                dbus_count += 1
                continue
            elif re_var_run_dbus.search(line):
                noutput_labelled[array_index].append('dbus')
                dbus_count += 1
                continue
            elif re_dbus_run_user.search(line):
                noutput_labelled[array_index].append('dbus')
                dbus_count += 1
                continue
            elif re_at_dbus.search(line):
                noutput_labelled[array_index].append('dbus')
                dbus_count += 1
                continue
            elif re_dbus_hyphen.search(line):
                noutput_labelled[array_index].append('dbus')
                dbus_count += 1
                continue
            elif re_x_related_x11.search(line):
                noutput_labelled[array_index].append('xrelated')
                xrelated_count += 1
                continue
            elif re_x_related_ice.search(line):
                noutput_labelled[array_index].append('xrelated')
                xrelated_count += 1
                continue
            elif re_connected_hyphen_end.search(line):
                noutput_labelled[array_index].append('nonidentified')
                nonidentified_count += 1
                continue
            elif re_dgram_hyphen_end.search(line):
                noutput_labelled[array_index].append('nonidentified')
                nonidentified_count += 1
                continue
            elif re_exim.search(line):
                noutput_labelled[array_index].append('exim')
                continue
            elif re_connected_numeric_master.search(line):
                noutput_labelled[array_index].append('postfix')
                postfix_count += 1
                continue
            elif re_listening_numeric_master.search(line):
                noutput_labelled[array_index].append('postfix')
                postfix_count += 1
                continue
            elif re_connected_rpc_dot.search(line):
                noutput_labelled[array_index].append('nfs')
                postfix_count += 1
                continue
            elif re_dgram_rpc_dot.search(line):
                noutput_labelled[array_index].append('nfs')
                postfix_count += 1
                continue
        else: # tcp or tcp6 or udp or udp6
            """ Here we deal with lines that are non re_unix_numeric which
            usually includes several lines which are shown LISTEN.
            This is why several of the regexes here are named ..._listen_...
            Example of daemons that are appropriate to put .search() in this
            section are mailers such as exim or postfix
            """
            if re_listen_sshd.search(line):
                noutput_labelled[array_index].append('sshd')
                ssh_count += 1
                continue
            elif re_listen_exim.search(line):
                noutput_labelled[array_index].append('exim')
                continue
            elif re_listen_master.search(line):
                noutput_labelled[array_index].append('postfix')
                postfix_count += 1
                continue
            elif re_rpc_dot_alpha.search(line):
                noutput_labelled[array_index].append('nfs')
                postfix_count += 1
                continue
            elif re_rpcbind.search(line):
                noutput_labelled[array_index].append('nfs')
                postfix_count += 1
                continue

        if len(noutput_labelled[array_index]) < 3:
            # Last gasp matching attempt on tcp or udp like
            if re_tcp6_numeric.search(line):
                noutput_labelled[array_index].append('tcp6')
            elif re_udp6_numeric.search(line):
                noutput_labelled[array_index].append('udp6')
            elif re_tcp_colons.search(line):
                noutput_labelled[array_index].append('tcp')
            elif re_udp_colons.search(line):
                noutput_labelled[array_index].append('udp')
            else:
                # Fall through as non of the 4 re above caught it
                noutput_labelled[array_index].append(UNLABELLED_STRING)

        if 'exim' in line and verbosity > 2:
                print(line,noutput_labelled[array_index])

        """ not interested in lines that match supplied globignore 
        # re_globignore.match(line) can be remembered as egrep '^needle'
        if re_globignore.search(line):
        continue
        """
            
    return noutput_label_rc


def noutput_list_filtered_by_label(label='UNLABELLED',verbosity=0):
    """ Returns a list of items from noutput_labelled filtered on
    the label you supply as first argument
    When verbosity >= 1 will also do a two field print() for you of each
    that passes the filter.
    """
    noutput_filtered = []
    for nout in noutput_labelled:
        if len(nout) >= 4:
            continue
        nout_label = nout[2]
        if nout_label != label:
            continue
	noutput_filtered.append(nout)
        if verbosity < 1:
            continue
        # We change the order during formatting so that 'input number' appears first in our printing
        print "{1} {0}".format(nout[0],nout[1])
    return noutput_filtered


def noutput_list_filtered_tcpudp(verbosity=0):
    """ Returns a list of items from noutput_labelled filtered so
    only retain tcp like or udp like
    When verbosity >= 1 will also do a two field print() for you of each
    that passes the filter.
    """
    noutput_filtered = []
    for nout in noutput_labelled:
        if len(nout) >= 4:
            continue
        nout_label = nout[2]
        if nout_label.startswith('tcp'):
            pass
        elif nout_label.startswith('udp'):
            pass
        else:
            continue
	noutput_filtered.append(nout)
        if verbosity < 1:
            continue
        # We change the order during formatting so that 'input number' appears first in our printing
        print "{1} {0}".format(nout[0],nout[1])
    return noutput_filtered


def noutput_report(verbosity=0):
    """ Typical print of all lines from noutput_labelled except
    those caught earlier in loop if tests below (exceptions)
    """
    if verbosity_global > 2:
        print("noutput_report({0}) called to iterate {1} lines of output".format(verbosity,
									len(noutput_labelled)))
    for n in noutput_labelled:
        if len(n) >= 4:
            continue
        label = n[2]
        if UNLABELLED_STRING == label:
            continue
        if ignore_exim_flag and label.startswith('exim'):
            continue
        if ignore_postfix_flag and label.startswith('postfix'):
            continue
        if ignore_nfs_flag and label.startswith('nfs'):
            continue
        if verbosity < 1:
            continue
        if 'warning' == label:
            # Avoid giving number to (and printing) lines labelled 'warning' in this loop
            continue
        if 'header' == label:
            # Avoid giving number to (and printing) lines labelled 'header' in this loop
            continue
        # We change the order during formatting so that 'input number' appears first in our printing
        print "{1} {0}".format(n[0],n[1])
    return


def noutput_report_unlabelled(verbosity=0):
    for n in noutput_labelled:
        if len(n) >= 4:
            continue
        label = n[2]
        if UNLABELLED_STRING != label:
            continue
        if verbosity < 1:
            continue
        # We change the order during formatting so that 'input number' appears first in our printing
        print "{1} {0}".format(n[0],n[1])
    return


def noutput_report_tcpudp(verbosity=0):
    noutput_list_filtered_tcpudp(verbosity)
    return


def report_detailed(verbosity=0):
    """ Calls noutput_report(1) during typical operation or based on
    flag values makes other output choices
    """
    if any127_flag:
        # User is interest in lines where in or out address is 127...
        for nlist in noutput_labelled:
	    if not re_any127.search(nlist[0].strip()):
    		continue
            # We change the order during formatting so that 'input number' appears first in our printing
            print "{1} {0}".format(nlist[0],nlist[1])		
    elif foreign127_flag:
        for nlist in noutput_labelled:
            if not re_foreign127.search(nlist[0].strip()):
                continue
            # We change the order during formatting so that 'input number' appears first in our printing
            print "{1} {0}".format(nlist[0],nlist[1])		
    elif tcpudp_flag:
        # User is interest in lines which are like tcp or like udp only
        noutput_report_tcpudp(1)
    elif unlabelledonly_flag:
        # User is interest in UNLABELLED and that is what we should output
        print("Operating in UNLABELLED mode so net output with label={0} only.".format(UNLABELLED_STRING))
        noutput_report_unlabelled(1)
    else:
        # More typical operation where we see things which could be labelled in some way
        noutput_report(1)
    return


if __name__ == '__main__':

    program_binary = argv[0].strip()

    parser = argparse.ArgumentParser()
    parser.add_argument('-n', action='store_true', default=False,
                        dest='log_update_flag',
                        help='Dry run - do not log results and/or update sqlite')
    parser.add_argument('--test', action='store_true', default=False,
                        dest='log_update_flag',
                        help='Dry run - do not log results and/or update sqlite')
    parser.add_argument('--stdin', action='store_true', default=False,
                        dest='stdin_flag',
                        help='Assume content of stdin is the output of netstat, rather than running it here.')
    parser.add_argument('--ignore', action='append', dest='ignore_list',
                        default=[],
                        help='Add network services / program identifiers to ignore',
                        )
    parser.add_argument('--archive', action='store', default=7,
                        dest='archive_days',
                        help='Maximum number of days to hold in interface_extra.csv. 7 or 1 are reasonable values.')
    parser.add_argument('--dbus11ignore', action='store_false', default=True,
                        dest='dbus11_flag',
                        help="""dbus11_flag is set false when we are given --dbus11ignore
    and means more filtering applied')""")
    parser.add_argument('--unlabelledonly', action='store_true', default=False,
                        dest='unlabelledonly_flag',
                        help='Print only those entries for which we assigned UNLABELLED.')
    parser.add_argument('--foreign127', action='store_true', default=False,
                        dest='foreign127_flag',
                        help="Print only those entries for which column 'foreign address' is 127.0.0.1.")
    parser.add_argument('--any127', action='store_true', default=False,
                        dest='any127_flag',
                        help="Print only those entries for which net address in or out is 127.0.0.1.")
    parser.add_argument('--tcpudp', action='store_true', default=False,
                        dest='tcpudp_flag',
                        help="Print only those entries for which are identified like tcp or like udp.")
    parser.add_argument('--crit', action='store', default=0,
                        dest='crit_thresh_int', type=int,
                        help="Critical threshold for producing concise one line output suitable for monitoring.")
    parser.add_argument('--warn', action='store', default=0,
                        dest='warn_thresh_int', type=int,
                        help="Warning threshold for producing concise one line output suitable for monitoring.")
    args = parser.parse_args()

    PYVERBOSITY_STRING = osgetenv('PYVERBOSITY')
    PYVERBOSITY = 1
    if PYVERBOSITY_STRING is None:
    	pass
    else:
	try:
		PYVERBOSITY = int(PYVERBOSITY_STRING)
	except:
		pass
    #print(PYVERBOSITY)

    """ Next verbosity_global = 1 is the most likely outcome
    unless the user specifically overrides it by setting
    the environment variable PYVERBOSITY
    """
    if PYVERBOSITY is None:
        verbosity_global = 1
    elif 0 == PYVERBOSITY:
        # 0 from env means 0 at global level
        verbosity_global = 0
    elif PYVERBOSITY > 1:
        # >1 from env means same at global level
        verbosity_global = PYVERBOSITY
    else:
        verbosity_global = 1
    

    archive_period_days = args.archive_days
    ignore_exim_flag = False
    if 'exim' in args.ignore_list:
        ignore_exim_flag = True
    ignore_postfix_flag = False
    if 'postfix' in args.ignore_list:
        ignore_postfix_flag = True
    ignore_nfs_flag = False
    if 'nfs' in args.ignore_list:
        ignore_nfs_flag = True
    unlabelledonly_flag = args.unlabelledonly_flag
    foreign127_flag = args.foreign127_flag
    any127_flag = args.any127_flag

    tcpudp_flag = args.tcpudp_flag
    """ onelinereport_flag is designed as an indicator of an output mode
    that is less verbose and more suitable for reading
    by a monitoring system such as icinga/nagios
    """
    onelinereport_flag = False
    crit_thresh_int = args.crit_thresh_int
    warn_thresh_int = args.warn_thresh_int
    if crit_thresh_int > 0 and warn_thresh_int > 0:
        # Implies onelinereport and that only interested in tcpudp
        onelinereport_flag = True
        tcpudp_flag = True
    
    if args.stdin_flag:
        process_count = process_from_stdin()
    else:
        process_count = process_from_command()
    
    if process_count > 0:
        netout_rc = noutput_label(verbosity_global)
        PROCESS_LABELLED_PREFIX="Processing net array length={0} next".format(len(noutput_labelled))
        if not onelinereport_flag and verbosity_global > 1:
            print "{0} (process_count={1}).".format(PROCESS_LABELLED_PREFIX,process_count)
        #if verbosity_global > 1 and netout_rc > 0:
        if netout_rc > 1 and verbosity_global > 1:
            PROCESS_NONZERO_RC_PREFIX='Warning: errors experienced during netstat'
            print "{0} (noutput_label returned {1})".format(PROCESS_NONZERO_RC_PREFIX,
								netout_rc)
    
    if dbus_count > 15:
        if not onelinereport_flag and verbosity_global > 0:
            print "dbus_count: {0} (non-zero) - probably a desktop.".format(dbus_count)

   
    #if not onelinereport_flag:
    if verbosity_global > 1 or not onelinereport_flag:
        """ Call reporting with verbosity=1 so some detailed output is displayed.
        When environment variable has been set to give more verbose, then we will
        give the detail even if onelinereport_flag is True
        """
        report_detailed(1)
    
    
    if verbosity_global > 2:
        WARNING_PREFIX_STR='Warnings detected from stdin / netstat'
        if warning_count > 0:
            print("{0} ({1} total) shown as list next.".format(WARNING_PREFIX_STR,
    							warning_count))
        for windex in warning_indexes:
            print "Warning: {0}".format(noutput_labelled[windex])
        noutput_list_filtered_by_label('warning',1)


    if onelinereport_flag is True:
        """ Override normal return code behaviour to ensure
        precise fit with what a monitoring system would expect.
        """
        OCC_OF_TU_LINES='occurrences of tcp or udp lines'
        list_filtered = noutput_list_filtered_tcpudp(0)
        lenf = len(list_filtered)
	perc_of_crit = 1
        SUFFIX_STR="{0} (??% of crit)".format(OCC_OF_TU_LINES)
	if lenf > 0:
            perc_of_crit = ((100*lenf)//crit_thresh_int)
            SUFFIX_STR="{0} ({1}% of crit)".format(OCC_OF_TU_LINES,perc_of_crit)
        if lenf >= crit_thresh_int:
            print("CRITICAL: {0} {1}".format(lenf,SUFFIX_STR))
            #print("CRITICAL: {0} {1}".format(lenf,OCC_OF_TU_LINES))
            exit(STATE_CRITICAL)
        elif lenf >= warn_thresh_int:
            #print("WARNING: {0} {1}".format(lenf,OCC_OF_TU_LINES))
            print("WARNING: {0} {1}".format(lenf,SUFFIX_STR))
            exit(STATE_WARNING)
        else:
            print("OK: {0} {1}".format(lenf,SUFFIX_STR))
            exit(STATE_OK)
            #pass
    

    if netout_rc > 1:
        # Error or other unexpected situation
        exit(netout_rc)
    elif netout_rc > 0:
        """ Warning for which if verbosity_global > 2 we already informed of
        Warning will not result in non-zero return code
        """
        pass
    else:
        # Neither error or warning indicated by netout_rc
        pass

    exit(0)

