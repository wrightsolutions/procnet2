# Monitoring/Nagios able to read daemon conf for maxconnections

The purpose of wrapper script ncon_from_conf_warnonmax1.py,
is to facilitate the generation of an appropriate --crit
value to the main ncon python script

Using typical 640 security a conf would be inaccessible to
a monitoring system.

The choice to open up your conf to your monitoring system
is one you will have to make based on your own setup/preferences

Being unable to read the conf would result in a return code > 2
( typically return code 102 )

More details on the failure might be available if prior to running
the script you manually issue:  
  export PYVERBOSITY=2


## Sudoable way using flag 'stdin'

There is another option available that may be more in keeping with
how you wish to limit access of the monitoring system.

An example next where we push the relevant conf extract directly
into the python script. Such a command can be added to sudo if this
fits better with how you integrate your monitoring.

egrep '^\s*maxconnections\s*=' /root/.pand/pand.conf | ./ncon_from_conf_warnonmax1.py 'stdin'

...or in more full form with a continuation (|)...
/bin/egrep '^\s*maxconnections\s*=' /root/.pand/pand.conf | \
/usr/lib/nagios/plugins/ncon_from_conf_warnonmax1.py 'stdin'

Sudoable your monitoring system might invoke sudo twice as follows:
sudo /bin/fgrep maxconnections /root/.pand/pand.conf | \
sudo /usr/bin/python2 /usr/lib/nagios/plugins/ncon_from_conf_warnonmax1.py 'stdin'


## Sudoable way using flag 'stdin' simulated input

printf "maxconnections=128" | /usr/bin/python2 /usr/lib/nagios/plugins/ncon_from_conf_warnonmax1.py 'stdin'

