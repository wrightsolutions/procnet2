#!/usr/bin/env python
# -*- coding: utf-8 -*-
from unittest import TestCase
from os import linesep
import re
from string import ascii_lowercase,digits,printable,punctuation

import ncon_filter2 as nc

class InputDesktop(TestCase):

	def setUp(self):

		# chr(33) is exclamation mark (!)      ; chr(34) is double quote (")     ; chr(39) is single quote (')
		# chr(35) is hash / octothorpe             ; chr(58) is colon (:)            ; chr(61) is equals (=)
		# chr(45) is hyphen			; chr(64) is at symbol (@)
		# chr(32) is space ; chr(10) is line feed

		self.SET_COLON_SPACE_EQUALS = set([chr(58),chr(32),chr(61)])
		self.SET_PRINTABLE = set(printable)
		self.SET_PUNCTUATION = set(punctuation)
		self.SET_LOWER_PLUS_DIGITS = set(ascii_lowercase).union(digits)

		self.STUFF1 = 'Some stuff'
		self.STUFF2 = 'More stuff'
		self.STUFF3 = 'Even more stuff'

		self.HEAD2NEWLINES = "{0}{0}".format(chr(10))
		self.HEAD2NEWLINES_SPLIT = ['','']
		#
		self.TAIL2NEWLINES = "{0}{1}{0}{1}".format(chr(32),chr(10))
		self.TAIL2NEWLINES_SPLIT = [' ',' ']
		#

		self.WARN2NONROOT = """
(Not all processes could be identified, non-owned process info
 will not be shown, you would have to be root to see it all.)
"""
		self.WARN2NONROOT_SPLIT = self.WARN2NONROOT.strip().split(linesep)

		self.TOTAL13UDP8_TEMPLATE = """
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      699/sshd            
tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      638/cupsd           
tcp6       0      0 :::22                   :::*                    LISTEN      699/sshd            
tcp6       0      0 ::1:631                 :::*                    LISTEN      638/cupsd           
udp        0      0 192.168.1.100:45939     {0}:53        ESTABLISHED 812/python          
udp        0      0 0.0.0.0:5353            0.0.0.0:*                           642/avahi-daemon: r 
udp        0      0 0.0.0.0:54567           0.0.0.0:*                           642/avahi-daemon: r 
udp        0      0 0.0.0.0:68              0.0.0.0:*                           1110/dhclient       
udp        0      0 0.0.0.0:631             0.0.0.0:*                           660/cups-browsed    
udp        0      0 0.0.0.0:1900            0.0.0.0:*                           1627/minissdpd      
udp6       0      0 :::35672                :::*                                642/avahi-daemon: r 
udp6       0      0 :::5353                 :::*                                642/avahi-daemon: r 
raw6       0      0 :::58                   :::*                    7           644/NetworkManager
"""

		self.DNS4 = '8.8.8.8'
		self.TOTAL13UDP8_SPLIT = (self.TOTAL13UDP8_TEMPLATE.format(self.DNS4)).split(linesep)


		self.TOTAL13UDP8 = self.TOTAL13UDP8_SPLIT[:]
		""" Next we form triple by just adding the head and tail to our already constructed double
		self.WARN2016TRIPLE_SPLIT = self.WARN2016DOUBLE_SPLIT[:]
		self.WARN2016TRIPLE_SPLIT.extend(self.WARNHEAD2016THREE_SPLIT[:])
		self.WARN2016TRIPLE_SPLIT.extend(self.TAIL2NEWLINES_SPLIT)
		self.WARN2016TRIPLE = '\n'.join(self.WARN2016TRIPLE_SPLIT)
		"""

		# Regular expressions are defined next and generally are named RE_...
		self.RE_ALNUMDASHAGENT = re.compile("[A-Za-z0-9]+{0}agent".format(chr(64)))
		self.RE_SPACEPLUSATFORWARD = re.compile("\s+{0}/".format(chr(64)))
		self.RE_RPCBIND = re.compile('rpcbind')
		#self.RE_ALNUMDASHAGENT = re.compile("[A-Za-z0-9]+{0}agent".format(chr(64)),re.IGNORECASE)

		# Next we have a commented out section useful if you want a copy of test data to /tmp/
		"""
		with open('/tmp/testdata.txt','w') as f:
			for line in self.TOTAL13UDP8_SPLIT:
				f.write("{0}\n".format(line))
			f.flush()
		# Copy of above self.TOTAL13UDP8_SPLIT is saved in input/total13udp8.txt
		"""
		return


	def test_warn_nonroot1(self):
		""" Test of correctly labelling lines of WARN2NONROOT
		"""
		self.assertEqual(len(self.WARN2NONROOT_SPLIT),2)
		nc.process_from_list(self.WARN2NONROOT_SPLIT)
		label_rc = nc.noutput_label(verbosity=3)
		# Using final arg of 1 instead of zero in next line will show you the warnings
		noutput_filtered_on_warning = nc.noutput_list_filtered_by_label('warning',0)
		self.assertEqual(len(noutput_filtered_on_warning),2)
		return


